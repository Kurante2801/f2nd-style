# F2nd Style
An unnoficial icon pack for Project Heartbeat meant to resemble Project Diva F 2nd.<br/>
<b>No Project Diva assets were used in the creation of this icon pack</b>, every icon is made from scratch using Affinity Photo and Affinity Designer.<br/>

## Installation
Clone or download this repository on the `resource_packs` folder located in the game's user folder.<br/>
For Windows users this folder will be inside `%APPDATA%/Project Heartbeat`<br/>

## Preview
![Main icons](editor_resources/background_main.png)
![Miscellaneous icons](editor_resources/background_misc.png)